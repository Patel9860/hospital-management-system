<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Home Page</title>
<style>
.box{
 width: 300px;
 padding: 40px;
 position: absolute;
 top: 50%;
 left: 50%;
 background: rgba(0, 0, 0, 0.5);
 color: #fff;
border-radius: 24px;
 transform: translate(-50%,-50%);
 opacity: 0.3
 text-align: center;
}
.box input[type = "text"],.box input[type = "password"]{
border: 0;
background: none;
display: block;
margin: 20px auto;
text-align: center;
border: 2px solid #3498db;
padding: 14px 10px;
width:200px;
outline: none;
color: white;
border-radius: 24px;
transition:0.25s; 
}

.box input[type = "text"]:focus,.box input[type = "password"]:focus{
width: 280px;
border-color: #2ecc71;
}

.box input[type = "submit"]{
border: 0;
background: none;
display: block;
margin: 20px auto;
text-align: center;
border: 2px solid #2ecc71;
padding: 14px 40px;
outline: none;
color: white;
border-radius: 24px;
transition: 0.25s;
cursor: pointer;
}
.box input[type = "submit"]:hover{
background: #2ecc71

}
  body {
	background-image:url("https://image.freepik.com/free-vector/hospital-clinic-building-with-ambulance-car-truck_107791-2645.jpg");
	background-repeat: no-repeat;
   background-attachment: fixed;
   background-size: 100% 100%;
	} 
</style>
</head>
<body>


	<h1 align="center" style="color: #ff4da6;">Welcome To HCL Healthcare</h1>
	
		
	
			
		<form class ="box" id="signin" action="login" method="post">
			${error}<br />

			<input type="text" name="userName" class="inputbox"
						autocomplete="off" autofocus="on" placeholder="Username"/>
			<input type="password" name="password" class="inputbox"
						autocomplete="off" autofocus="on" placeholder="Password" />
					<input type="submit" value="login" class="submitButton" />
		</form>
	
</body>
</html>
