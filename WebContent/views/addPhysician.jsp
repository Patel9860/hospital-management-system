<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Physiain</title>
<style>
.button {
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 8px 2px;
  cursor: pointer;
}
.button1 {background-color: #4CAF50;}
.button2 {background-color: #4CAF50;}
.box {
	width: 300px;
	padding: 10px;
	position: absolute;
	top: 50%;
	left: 50%;
	background: rgba(0, 0, 0, 0.5);
	color: #fff;
	border-radius: 24px;
	transform: translate(-50%, -50%);
	opacity: 0.3 text-align: center;
}
.box input[type = "text"]{
border: 0;
background: none;
display: block;
margin: 8px auto;
text-align: center;
border: 2px solid #3498db;
padding: 14px 10px;
width:200px;
outline: none;
color: white;
border-radius: 24px;
transition:0.25s; 
}

.box input[type = "text"]:focus{
width: 280px;
border-color: #2ecc71;
}

.box input[type = "submit"]{
border: 0;
background: none;
display: block;
margin: 20px auto;
text-align: center;
border: 2px solid #2ecc71;
padding: 14px 40px;
outline: none;
color: white;
border-radius: 24px;
transition: 0.25s;
cursor: pointer;
}
.box input[type = "submit"]:hover{
background: #2ecc71

}
.box input[type = "reset"]{
border: 0;
background: none;
display: block;
margin: 20px auto;
text-align: center;
border: 2px solid #2ecc71;
padding: 14px 40px;
outline: none;
color: white;
border-radius: 24px;
transition: 0.25s;
cursor: pointer;
}
.box input[type = "reset"]:hover{
background: #2ecc71

}

body {
	background-image:
		url("https://image.freepik.com/free-photo/young-handsome-physician-medical-robe-with-stethoscope_1303-17818.jpg");
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: 100% 100%;
}
</style>
</head>
<body>
<button class="button button1" >
			<a href="index.jsp" style="text-decoration: none;">Home</a>
		</button>
		<button class="button button2" >
			<a href="admin.jsp" style="text-decoration: none;">Admin</a>
		</button>
	
	<br />
	<h1 align="left" style="color: #003300;">Add Physician Here</h1>
	

	<div align="center">

		<s:form class="box" action="adding" method="post" modelAttribute="p">

			<s:input type="text" path="physicianId" autocomplete="off" readonly="readonly"
				placeholder="Physician ID" />

			<s:input type="text" path="physicianFirstName" autocomplete="off"
				placeholder="Physician FirstName" />


			<s:input type="text" path="physicianLastName" autocomplete="off"
				placeholder="Physician LastName" />

			<s:input type="text" path="department" autocomplete="off"
				placeholder="Department" />

			<s:input type="text" path="educationalQualification" autocomplete="off"
				placeholder="Educational Qualification" />

			<s:input type="text" path="yearsOfExperience" autocomplete="off"
				placeholder="Experience" />

			<s:input type="text" path="state" autocomplete="off" placeholder="State" />

			<s:input type="text" path="insurancePlan" autocomplete="off"
				placeholder="Insurance Plan" />

			<input type="submit" value="Register" />

			<input type="reset" value="Reset" />


		</s:form>
	</div>
</body>
</html>
