<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin page</title>
<style>
.button {
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 8px 2px;
  cursor: pointer;
}
.button1 {background-color: #4CAF50;}
.button2 {background-color: #008CBA;}  
.button3 {background-color: #4CAF50;}
.button4 {background-color: #008CBA;} 
.button5 {background-color: #4CAF50;}
.button6 {background-color: #008CBA;} 
.button:hover {
  background-color: #4CAF50;
}  
body {
	background-image:
		url("https://image.freepik.com/free-vector/healthcare-background-with-medical-symbols-hexagonal-frame_1017-26363.jpg");
		background-repeat: no-repeat;
   background-attachment: fixed;
   background-size: 100% 100%;
}



</style>
</head>
<body>
	<h1 align="center" style="color: #ccff66;">Welcome to the Admin
		Page</h1>
	<hr />
	
<div align="center" >
	<form class ="box" id="select" action="next" method="post">
		
		<button class="button button1" >
			<a href="addPhysician" style="text-decoration: none;">Add Physician</a>
		</button>
		<br />
		<button class="button button2">
			<a href="enrollPatient" style="text-decoration: none;">Enroll Patient</a>
		</button>
		<br />
		<button class="button button3">
			<a href="patientDiagnosisDetails" style="text-decoration: none;">PatientDiagnosisDetails</a>
		</button>
		<br />
		<button class="button button4">
			<a href="searchPhysician" style="text-decoration: none;">Search Physician</a>
		</button>
		<br />
		<button class="button button5">
			<a href="viewHistory" style="text-decoration: none;">View History</a>
		</button>
		<br />
		<button class="button button6">
			<a href="index.jsp" style="text-decoration: none;">Home</a>
		</button>	
	</form>
	</div>
</body>
</html>
