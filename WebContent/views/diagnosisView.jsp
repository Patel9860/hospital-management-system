<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
</head>
<body>
	<h1 align="center" style="color: #ff4da6;">Patient Diagnosis Details Preview</h1>
	<hr />
	<a href="index.jsp">Home</a>
	
	<br />
	<br />
	<table border="1"
				style="with: 50%; margin-left: auto; margin-right: auto;">
		<tr style="color:#993333;">
			<td>Patient ID</td>
			<td>${p.getPatientId()}</td>
		<tr />
		<tr style="color:#993333;">
			<td>Diagnosis ID</td>
			<td>${p.getDiagnosisId()}</td>
		<tr />
		<tr style="color:#993333;">
			<td>Symptoms</td>
			<td>${p.getSymptoms()}</td>
		<tr />
		<tr style="color:#993333;">
			<td>Diagnosis Provided</td>
			<td>${p.getDiagnosisProvided()}</td>
		<tr />
		<tr style="color:#993333;">
			<td>Adminstered By</td>
			<td>${p.getAdministeredBy()}</td>
		<tr />
		<tr style="color:#993333;">
			<td>Date Of Diagnosis</td>
			<td>${p.getDateOfDiagnosis()}</td>
		<tr />
		<tr style="color:#993333;">
			<td>Followup Required</td>
			<td>${p.getFollowUpRequired()}</td>
		<tr />
		<tr style="color:#993333;">
			<td>Date Of Follow Up</td>
			<td>${p.getDateOfFollowUp()}</td>
		<tr />
		<tr style="color:#993333;">
			<td>Bill Amount</td>
			<td>${p.getBillAmount()}</td>
		<tr />
		<tr style="color:#993333;">
			<td>Card Number</td>
			<td>${p.getCardNumber()}</td>
		<tr />
		<tr style="color:#993333;">
			<td>Mode Of Amount</td>
			<td>${p.getModeOfAmount()}</td>
		</tr>

	</table>
</body>
</html>
