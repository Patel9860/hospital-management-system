<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Success Page</title>
<style>
.button {
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 8px 2px;
  cursor: pointer;
}
.button1 {background-color: #4CAF50;}
.button2 {background-color: #4CAF50;}
.styled-table {
	border-collapse: collapse;
	margin: 25px 0;
	font-size: 0.9em;
	font-family: sans-serif;
	min-width: 400px;
	box-shadow: 0 0 20px rgba(0, 0, 0, 0.15);
}
.styled-table thead tr {
	background-color: #009879;
	color: #ffffff;
	text-align: left;
}
.styled-table th, .styled-table td {
	padding: 12px 15px;
}
.styled-table tbody tr {
	border-bottom: 1px solid #dddddd;
}
.styled-table tbody tr:nth-of-type(even) {
	background-color: #f3f3f3;
}
.styled-table tbody tr:last-of-type {
	border-bottom: 2px solid #009879;
}
body {
	background-image:
		url("https://image.freepik.com/free-vector/hand-drawn-hospital-reception-scene_52683-55314.jpg");
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: 100% 100%;
}
</style>
</head>
<body>
<button class="button button1">
		<a href="index.jsp" style="text-decoration: none;">Home</a>
	</button>
	<button class ="button button2">
	<a href="admin.jsp" style="text-decoration: none;">Admin</a>
	</button>
	<h1 align="center" style="color: #ff4da6;">Patient Details Preview</h1>
	
	<div align="center" >
	<table class="styled-table">
		<thead>
			<tr>
				<th>Patient ID</th>
				<th>FirstName</th>
				<th>LastName</th>
				<th>Password</th>
				<th>Date of Birth</th>
				<th>Email Address</th>
				<th>Contact Number</th>
				<th>State</th>
				<th>Insurance Plan</th>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>${p.getPatientId()}</td>
				<td>${p.getFirstName()}</td>
				<td>${p.getLastName()}</td>
				<td>${p.getPassword()}</td>
				<td>${p.getDateOfBirth()}</td>
				<td>${p.getEmail()}</td>
				<td>${p.getContactNumber()}</td>
				<td>${p.getState()}</td>
				<td>${p.getInsurancePlan()}</td>
			<tr />

		</tbody>
	</table>
</div>
	
	<h4 align="center" style="color: #ff4da6;">Registered Successfully</h4>
</body>
</html>
