<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Register</title>
<style>
.button {
  border: none;
  color: white;
  padding: 10px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  margin: 8px 2px;
  cursor: pointer;
}
.button1 {background-color: #4CAF50;}
.button2 {background-color: #4CAF50;}
.button3 {background-color: #008CBA;}
.box {
	width: 300px;
	padding: 10px;
	position: absolute;
	top: 50%;
	left: 50%;
	
	color: #fff;
	border-radius: 24px;
	transform: translate(-50%, -50%);
	opacity: 0.5 text-align: center;
}
.box input[type = "text"]{
border: 0;
background: none;
display: block;
margin: 1px auto;
text-align: center;
border: 2px solid #3498db;
padding: 5px 20px;
width:200px;
outline: none;
color: white;
border-radius: 24px;
transition:0.25s; 
}

.box input[type = "text"]:focus{
width: 280px;
border-color: #2ecc71;
}
body {
	background-image:
		url("https://image.freepik.com/free-photo/abstract-blur-hospital-clinic-interior_74190-4854.jpg");
	background-repeat: no-repeat;
	background-attachment: fixed;
	background-size: 100% 100%;
}
</style>
</head>
<body>
	<button class="button button1">
		<a href="index.jsp" style="text-decoration: none;">Home</a>
	</button>
	<button class="button button2">
		<a href="admin.jsp" style="text-decoration: none;">Admin</a>
	</button>

	<h1 align="center" style="color: #003366;">Enroll Patient Details</h1>

	<div align="center">
		<s:form class="box" action="Register" method="post" modelAttribute="p">

			<s:input type="text" path="patientId" autocomplete="off"
				placeholder="Patient ID" />

			<s:input type="text" path="firstName" autocomplete="off"
				placeholder="First Name" />

			<s:input type="text" path="lastName" autocomplete="off"
				placeholder="Last Name" />

			<s:input type="text" path="password" autocomplete="off"
				placeholder="Password" />
				
			<s:input type="text" path="dateOfBirth" autocomplete="off"
				placeholder="Date Of Birth" />

			<s:input type="text" path="email" autocomplete="off"
				placeholder="Email" />

			<s:input type="text" path="contactNumber" autocomplete="off"
				placeholder="Contact Number" />

			<s:input type="text" path="state" autocomplete="off"
				placeholder="State" />

			<s:input type="text" path="insurancePlan" autocomplete="off"
				placeholder="Insurance Plan" />

			<s:button class="button button3">Register</s:button>
			

		
		</s:form>
	</div>
</body>
</html>
