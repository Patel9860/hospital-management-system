<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://www.springframework.org/tags/form" prefix="s"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Diagnosis Details</title>
<style type="text/css">
body{
background-color:#ffffcc;
}
</style>
</head>
<body>
<h1 align="center" style="color: #ff4da6;">Enroll Patient Diagnosis Details</h1><hr/>
	
	<s:form action="addDD" method="post" modelAttribute="p">
		<table border="1"
				style="with: 50%; margin-left: auto; margin-right: auto;">
			<tr style="color:#003366;">
				<td>Patient ID :</td>
				<td><s:input path="patientId" autocomplete="off" /></td>
			</tr>
			<tr style="color:#003366;">
				<td>Diagnosis ID :</td>
				<td><s:input path="diagnosisId" autocomplete="off" /></td>
			</tr>
			<tr style="color:#003366;">
				<td>Symptoms:</td>
				<td><s:input path="symptoms" autocomplete="off" /></td>
			</tr>
			<tr style="color:#003366;">
				<td>Diagnosis Provided :</td>
				<td><s:input path="diagnosisProvided" autocomplete="off" /></td>
			</tr>
			<tr style="color:#003366;">
				<td>Adminstered By:</td>
				<td><s:input path="administeredBy" autocomplete="off" /></td>
			</tr>
			<tr style="color:#003366;">
				<td>Date Of Diagnosis :</td>
				<td><s:input path="dateOfDiagnosis" autocomplete="off" /></td>
			</tr>
			<tr style="color:#003366;">
				<td>Followup Required :</td>
				<td><s:input path="followUpRequired" autocomplete="off" /></td>
			</tr>
			<tr style="color:#003366;">
				<td>Date Of Follow Up :</td>
				<td><s:input path="dateOfFollowUp" autocomplete="off" /></td>
			</tr>
			<tr style="color:#003366;">
				<td>Bill Amount :</td>
				<td><s:input path="billAmount" autocomplete="off" /></td>
			</tr>
			<tr style="color:#003366;">
				<td>Card Number :</td>
				<td><s:input path="cardNumber" autocomplete="off" /></td>
			</tr>
			<tr style="color:#003366;">
				<td>Mode Of Payment :</td>
				<td><s:input path="modeOfAmount" autocomplete="off" /></td>
			</tr>
			<tr>
				<td></td>
				<td><s:button>Save</s:button></td>
			</tr>
		</table>
	</s:form>
</body>
</html>
