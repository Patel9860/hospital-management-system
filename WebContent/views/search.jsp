<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Search Details</title>
</head>
<body>
	<h1 align="center" style="color: #ff4da6;">View Deatils Of Physician</h1><hr/>

	<a href="index.jsp">Home</a>
	<br/><br/>
	
	<table border="1"
				style="with: 50%; margin-left: auto; margin-right: auto;">
	<tr style="color:#993333;">
	<th>Physician ID</th>
	<th>Physician First Name</th>
	<th>Physician Last Name</th>
	<th>Department</th>
	<th>Educational Qualification</th>
	<th>Experience</th>
	<th>State</th>
	<th>Insurance Plan</th>
	</tr>
	<c:forEach items="${p}" var="p">
	<tr style="color:#993333;"> 
	<td>${p.getPhysicianId()}</td>
	<td>${p.getPhysicianFirstName()}</td>
	<td>${p.getPhysicianLastName()}</td>
	<td>${p.getDepartment()}</td>
	<td>${p.getEducationalQualification()}</td>
	<td>${p.getYearsOfExperience()}</td>
	<td>${p.getState()}</td>
	<td>${p.getInsurancePlan()}</td>
	</tr>
	</c:forEach>
	</table>
	
	<h4 align="center" style="color: #ff4da6;">Successfully Searched</h4>
</body>
</html>
