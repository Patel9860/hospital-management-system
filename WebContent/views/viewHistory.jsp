<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>view history</title>
<style type="text/css">
body {
	background-color:#ccffff;
	}

p {
	color: blue;
	text-align: justify;
	font-family: Comic sans MS;
}
</style>
</head>
<body>
	<h1 align="center" style="color: #ff4da6;">View History Of patient Here</h1><hr/>
	
	<a href="index.jsp">Home</a>
	<br/><br/>
	<table border="1"
				style="with: 50%; margin-left: auto; margin-right: auto;">
	<tr style="color:#993333;">
	<th>Patient ID</th>
	<th>First Name</th>
	<th>Last Name</th>
	</tr style="color:#993333;">
	<c:forEach items="${p}" var="p">
	<tr> 
	<td>${p.getPatientId()}</td>
	<td>${p.getFirstName()}</td>
	<td>${p.getLastName()}</td>
	</tr>
	</c:forEach>
	</table>

</body>
</html>
