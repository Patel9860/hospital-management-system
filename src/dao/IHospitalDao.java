package dao;

import java.util.List;

import model.Admin;
import model.Patient;
import model.PatientDiagnosisDetails;
import model.Physician;

public interface IHospitalDao {
  public boolean adminAuthentication(Admin admin);
  public void register(Patient p);
  public void addPhysician(Physician p);
  public List<Physician> searchPhysician(Physician p);
  public void save(PatientDiagnosisDetails p);
  public List<Patient> viewPatientHistory();

}
