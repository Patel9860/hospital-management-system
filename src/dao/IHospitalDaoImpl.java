package dao;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import model.Admin;
import model.Patient;
import model.PatientDiagnosisDetails;
import model.Physician;

public class IHospitalDaoImpl implements IHospitalDao {

 

  @SuppressWarnings({ "rawtypes"})
  @Override
  public boolean adminAuthentication(Admin admin) {
   
    Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().getCurrentSession();
    session.beginTransaction();
    boolean isValid=false;
    
    Query q = session.createQuery("from Admin where userName=?1 and password=?2");
    q.setParameter(1,admin.getUserName());
    q.setParameter(2,admin.getPassword());
    
    List list=q.list();
    if(list!=null && list.size()>0) {
      isValid=true;
    }
    
    return isValid;
  }
  @Override
  public void register(Patient p) {
    Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().getCurrentSession();
    Transaction tx=session.beginTransaction();
    session.save(p);
    tx.commit();
  }
  @Override
  public void addPhysician(Physician p) {
    Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().getCurrentSession();
    Transaction tx=session.beginTransaction();
    session.save(p);
    tx.commit();
    
  }

  @Override
  public void save(PatientDiagnosisDetails pd) {
    Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().getCurrentSession();
    Transaction tx=session.beginTransaction();
    session.save(pd);
    tx.commit();
    
  }
  @SuppressWarnings({ "unchecked", "rawtypes" })
  @Override
  public List<Patient> viewPatientHistory() {
    Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().getCurrentSession();
    session.beginTransaction();
    List<Patient> list=new ArrayList<Patient>();
    Query q= session.createQuery("from Patient");
    List<Patient> li = q.list();
    for (Patient patient : li) {
      list.add(patient);
    }
    return list;
  }
  @SuppressWarnings({ "rawtypes", "unchecked" })
  @Override
  public List<Physician> searchPhysician(Physician p) {
    Session session=new Configuration().configure("hibernate.cfg.xml").buildSessionFactory().getCurrentSession();
    session.beginTransaction();
    List<Physician> list=new ArrayList<Physician>();
    Query q=session.createQuery("from Physician where state=?1 and insurancePlan=?2 and Department=?3");
    q.setParameter(1,p.getState());
    q.setParameter(2, p.getInsurancePlan());
    q.setParameter(3,p.getDepartment());
    List<Physician> li=q.list();
    for (Physician physician : li) {
      list.add(physician);
      
    }
    return list;
  }
 
 
}
