package model;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;

@Entity
@Inheritance(strategy=InheritanceType.JOINED)
public class Patient {
  @Id
  private Integer patientId;
  private String firstName;
  private String lastName;
  private String password;
  private String dateOfBirth;
  private String email;
  private long contactNumber;
  private String state;
  private String insurancePlan;
  
 public Patient() {
  // TODO Auto-generated constructor stub
}


  public Patient(Integer patientId) {
    super();
    this.patientId = patientId;
  }



  public Patient(Integer patientId, String firstName, String lastName,
      String password, String dateOfBirth, String email, long contactNumber,
      String state, String insurancePlan) {
    super();
    this.patientId = patientId;
    this.firstName = firstName;
    this.lastName = lastName;
    this.password = password;
    this.dateOfBirth = dateOfBirth;
    this.email = email;
    this.contactNumber = contactNumber;
    this.state = state;
    this.insurancePlan = insurancePlan;
  }


  public Integer getPatientId() {
    return patientId;
  }


  public void setPatientId(Integer patientId) {
    this.patientId = patientId;
  }


  public String getFirstName() {
    return firstName;
  }


  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }


  public String getLastName() {
    return lastName;
  }


  public void setLastName(String lastName) {
    this.lastName = lastName;
  }


  public String getPassword() {
    return password;
  }


  public void setPassword(String password) {
    this.password = password;
  }


  public String getDateOfBirth() {
    return dateOfBirth;
  }


  public void setDateOfBirth(String dateOfBirth) {
    this.dateOfBirth = dateOfBirth;
  }


  public String getEmail() {
    return email;
  }


  public void setEmail(String email) {
    this.email = email;
  }


  public long getContactNumber() {
    return contactNumber;
  }


  public void setContactNumber(long contactNumber) {
    this.contactNumber = contactNumber;
  }


  public String getState() {
    return state;
  }


  public void setState(String state) {
    this.state = state;
  }


  public String getInsurancePlan() {
    return insurancePlan;
  }


  public void setInsurancePlan(String insurancePlan) {
    this.insurancePlan = insurancePlan;
  }
  

  
}
