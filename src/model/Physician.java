package model;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
public class Physician {
  @Id
  private String physicianId;
  private String physicianFirstName;
  private String physicianLastName;
  private String department;
  private String educationalQualification;
  private Integer yearsOfExperience;
  private String state;
  private String insurancePlan;
  
  public Physician() {
    // TODO Auto-generated constructor stub
  }

  public Physician(String physicianId, String physicianFirstName,
      String physicianLastName, String department,
      String educationalQualification, Integer yearsOfExperience, String state,
      String insurancePlan) {
    super();
    this.physicianId = physicianId;
    this.physicianFirstName = physicianFirstName;
    this.physicianLastName = physicianLastName;
    this.department = department;
    this.educationalQualification = educationalQualification;
    this.yearsOfExperience = yearsOfExperience;
    this.state = state;
    this.insurancePlan = insurancePlan;
  }

  public String getPhysicianId() {
    return physicianId;
  }

  public void setPhysicianId(String physicianId) {
    this.physicianId = physicianId;
  }

  public String getPhysicianFirstName() {
    return physicianFirstName;
  }

  public void setPhysicianFirstName(String physicianFirstName) {
    this.physicianFirstName = physicianFirstName;
  }

  public String getPhysicianLastName() {
    return physicianLastName;
  }

  public void setPhysicianLastName(String physicianLastName) {
    this.physicianLastName = physicianLastName;
  }

  public String getDepartment() {
    return department;
  }

  public void setDepartment(String department) {
    this.department = department;
  }

  public String getEducationalQualification() {
    return educationalQualification;
  }

  public void setEducationalQualification(String educationalQualification) {
    this.educationalQualification = educationalQualification;
  }

  public Integer getYearsOfExperience() {
    return yearsOfExperience;
  }

  public void setYearsOfExperience(Integer yearsOfExperience) {
    this.yearsOfExperience = yearsOfExperience;
  }

  public String getState() {
    return state;
  }

  public void setState(String state) {
    this.state = state;
  }

  public String getInsurancePlan() {
    return insurancePlan;
  }

  public void setInsurancePlan(String insurancePlan) {
    this.insurancePlan = insurancePlan;
  }
  

}
